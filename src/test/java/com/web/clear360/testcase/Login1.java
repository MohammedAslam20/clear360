package com.web.clear360.testcase;

import java.awt.AWTException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;


public class Login1 {
	
	WebDriver driver;
	public Select depttype;

		@BeforeTest
		public void Browser() {
			
			System.setProperty("webdriver.chrome.driver","F:\\Mercury\\chromedriver.exe"); 
			driver = new ChromeDriver();
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		}

		@Test
		public void validLogin() {
			driver.get("https://dev.clear360.com/");  
			driver.findElement(By.id("exampleInputEmail")).sendKeys("mohammed.aslam@aurigait.com");
			driver.findElement(By.id("exampleInputPassword")).sendKeys("Aslam123*");
			driver.findElement(By.xpath("//button[@type='submit']")).click();
		}
		
		
		@Test(priority = 1)
		public void createdept_validvalues() {
			Actions builder = new Actions(driver);
			WebElement hover = driver.findElement(By.xpath("//div[@class='avatar-w']//i[@class='os-icon os-icon-menu']"));
			builder.moveToElement(hover).perform();
			WebElement clk_on_scl = driver.findElement(By.xpath("//div[@class='logged-user-i logged-user-i1']//span[contains(text(),'Grades')]"));
			builder.moveToElement(clk_on_scl).click().perform();
			driver.findElement(By.xpath("//button[contains(text(),'Create New Grade')]")).click();
		
			driver.findElement(By.id("depName")).sendKeys("aslam");
			driver.findElement(By.id("depCode")).sendKeys("551");
			depttype = new Select(driver.findElement(By.id("depType")));
			depttype.selectByVisibleText("Executive");
			driver.findElement(By.xpath("//button[contains(text(),'Submit')]")).click();
		}
			

		@AfterTest
		public void closeBrowser() {
			driver.close();
		}
	}

