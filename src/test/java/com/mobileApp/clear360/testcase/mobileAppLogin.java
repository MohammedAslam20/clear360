package com.mobileApp.clear360.testcase;



import java.io.File;
import java.io.IOException;
//import java.io.File;
import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.mobileApp.clear360.utility.baseClassMobile;

//import org.openqa.selenium.By;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;


//Inherit the property from parent class with extends keyword

public class mobileAppLogin extends baseClassMobile {

	//public static AndroidDriver<AndroidElement> driver;
	
	/*
	 * File f = new File ("src"); //File fs = new File(f,
	 * "Guardian_July14__stage.apk"); File fs = new File(f,
	 * "Clear360EduMVP_dev_Sep22.apk");
	 */
	

	@Test(priority =1)
	public void login() throws MalformedURLException, InterruptedException  {

		// TODO Auto-generated method stub

		//parent class methods "caps" use in child class "twcLogin"

		
		
		
		// TODO Auto-generated method stub

		//parent class methods "caps" use in child class "twcLogin"

		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		//Click on "Skip"
		driver.findElementByXPath("//android.widget.TextView[contains(@text,'SKIP')]").click();


		//for wrong credentials (Negative testing)
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElementByXPath("//*[@class='android.widget.EditText' and @index='0']").sendKeys("12345");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		driver.findElementByXPath("//android.widget.TextView[contains(@text,'Next')]").click();

		System.out.println ("Please enter valid organization id");


		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		//Enter Invalid Mobile no.
		driver.findElementByXPath("//*[@class='android.widget.EditText' and @index='2']").sendKeys("8965231");
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

		driver.findElementByXPath("//*[@class='android.view.ViewGroup' and @index='0']").click();

		System.out.println ("Please enter valid Phone Number");

		//Enter un-associated mobile no.
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElementByXPath("//*[@class='android.widget.EditText' and @index='0']").sendKeys("10000043-101-102");
		driver.findElementByXPath("//*[@class='android.widget.EditText' and @index='2']").sendKeys("9024429791");
		System.out.println ("Your account is not associated with any organization id");


		//Successfully login positive testcase
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		//driver.findElementByXPath("//*[@class='android.widget.EditText' and @index='0']").sendKeys("10000020-101-101");
		driver.findElementByXPath("//*[@class='android.widget.EditText' and @index='0']").sendKeys("10000082");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);


		driver.findElementByXPath("//*[@class='android.widget.Spinner' and @index='0']").click();
		//driver.findElementByXPath("//*[@class='android.widget.CheckedTextView' and @index='0']").click();
		//driver.findElementByXPath("//*[@class='android.widget.EditText' and @index='2']").sendKeys("9887808874");
		driver.findElementByXPath("//*[@class='android.widget.CheckedTextView' and @text='+91']").click();
		driver.findElementByXPath("//*[@class='android.widget.EditText' and @index='2']").sendKeys("9024429799");
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		driver.findElementByXPath("//*[@class='android.widget.TextView' and @text='Enter Mobile Number']").click();
		driver.findElementByXPath("//*[@class='android.widget.TextView' and @text='Next']").click();
		//driver.findElementByXPath("//*[@class='android.view.ViewGroup' and @index='0']").click();
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		System.out.println ("Successfully login");

		driver.manage().timeouts().implicitlyWait(100,TimeUnit.SECONDS);

		System.out.println("Please Enter OTP");


		Thread.sleep(8000);
		//driver.findElementByXPath("//*[@class='android.widget.EditText' and @index='0']").sendKeys("1");

		/*
		 * Thread.sleep(5000); driver.
		 * findElementByXPath("//*[@class='android.widget.EditText' and @index='1']").
		 * sendKeys("2"); Thread.sleep(5000); driver.
		 * findElementByXPath("//*[@class='android.widget.EditText' and @index='2']").
		 * sendKeys("3"); Thread.sleep(5000); driver.
		 * findElementByXPath("//*[@class='android.widget.EditText' and @index='3']").
		 * sendKeys("4"); Thread.sleep(5000); driver.
		 * findElementByXPath("//*[@class='android.widget.EditText' and @index='4']").
		 * sendKeys("5"); Thread.sleep(5000); driver.
		 * findElementByXPath("//*[@class='android.widget.EditText' and @index='5']").
		 * sendKeys("6");
		 */
		Thread.sleep(8000);

		
		
		driver.findElementByXPath("//*[@class='android.widget.TextView' and @text='Next']").click();
		
		driver.findElementByXPath("//*[@class='android.widget.TextView' and @text='Enter the 6 Digit Code \r\n" + 
				" We Sent You']").click();
		driver.findElementByXPath("//*[@class='android.widget.TextView' and @text='Login Without OTP']").click();
		
		
		
		driver.manage().timeouts().implicitlyWait(50,TimeUnit.SECONDS);
		System.out.println ("OTP Verify Successfully");





		//Verify Screen
		Thread.sleep(5000);
		driver.findElementByXPath("//*[@class='android.widget.TextView' and @index='1']").getText();
		driver.findElementByXPath("//*[@class='android.widget.TextView' and @index='3']").getText();
		
		driver.findElementByXPath("//*[@class='android.view.ViewGroup' and @index='6']").click();
		
		driver.findElementByXPath("//*[@class='android.widget.TextView' and @text='Next']").click();
		Thread.sleep(3000);
		System.out.println ("Congrats you're Verified");
		Thread.sleep(3000);


		//Click on "I Accept" button
		driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
		driver.findElementByXPath("//*[@class='android.widget.TextView' and @text='I Accept']").click();

		System.out.println ("Accept the Terms so Clear360 Can Start Protecting You");

		driver.manage().timeouts().implicitlyWait(50,TimeUnit.SECONDS);
		driver.findElementByXPath("//*[@class='android.widget.TextView' and @text='Enable Notifications']").click();
		System.out.println("Enable Notifications Successfully");
		
		
		
		
		driver.findElementByXPath("//*[@class='android.widget.TextView' and @index='3']").getText();
		
		driver.findElementByXPath("//*[@class='android.widget.TextView' and @text='Next']").click();


	
          //Check-in
		
		driver.findElementByXPath("//*[@class='android.widget.TextView' and @index='0']").getText();
		driver.findElementByXPath("//*[@class='android.widget.TextView' and @index='1']").getText();
		driver.findElementByXPath("//*[@class='android.widget.TextView' and @text='0']").getText();
		driver.findElementByXPath("//*[@class='android.widget.TextView' and @text='Next']").click();
		driver.findElementByXPath("//*[@class='android.widget.TextView' and @text='Next']").click();
		driver.findElementByXPath("//*[@class='android.widget.TextView' and @text='Next']").click();



		//Save Profile 

		Thread.sleep(5000);
	    
	    driver.findElementByXPath("//*[@class='android.widget.EditText' and @text='Mohammed']").clear();
	    Thread.sleep(4000);
	    driver.findElementByXPath("//*[@class='android.widget.EditText' and @text='Aslam']").clear();
	    Thread.sleep(4000);
	    driver.findElementByXPath("//*[@class='android.widget.EditText' and @text='25']").clear();
	    
	    
		//driver.findElementByXPath("//*[@class='android.widget.EditText' and @bounds='[609,501][1314,672]']").clear();
		//driver.findElementByXPath("//*[@class='android.widget.EditText' and @bounds='[609,763][1314,934]']").clear();
		//driver.findElementByXPath("//*[@class='android.widget.EditText' and @bounds='[609,1025][1314,1196]']").clear();


		driver.findElementByXPath("//*[@class='android.widget.TextView' and @text='Save Profile']").click();
		System.out.println ("All fields are mandatory First name,last name,age,biological sex");


		//First Name Validation
		driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
		driver.findElementByXPath("//*[@class='android.widget.EditText' and @text='Enter first name here']").sendKeys("123firstname");
		driver.findElementByXPath("//*[@class='android.widget.TextView' and @text='Save Profile']").click(); 
		System.out.println ("Please enter a valid name:must conation between 2-40 characters"); 

		driver.findElementByXPath("//*[@class='android.widget.EditText' and @text='123firstname']").sendKeys("Mohammed");

		//Last Name Validation
		driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
		driver.findElementByXPath("//*[@class='android.widget.EditText' and @text='Enter last name here']").sendKeys("123Lastname");
		driver.findElementByXPath("//*[@class='android.widget.TextView' and @text='Save Profile']").click(); 

		System.out.println ("Please enter a valid Last name:must conation between 2-40 characters"); 


		driver.findElementByXPath("//*[@class='android.widget.EditText' and @text='123Lastname']").sendKeys("Aslam");

		//Age Validation


		driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
		driver.findElementByXPath("//*[@class='android.widget.EditText' and @text='Enter your age here']").sendKeys("0"); 
		driver.findElementByXPath("//*[@class='android.widget.TextView' and @text='Save Profile']").click();
		System.out.println("Age field should not accept 0 , Please enter valid Age");
		driver.findElementByXPath("//*[@class='android.widget.EditText' and @text='0']").sendKeys("25");


		//Fill valid data
		Thread.sleep(8000);
		/*
		 * driver.
		 * findElementByXPath("//*[@class='android.widget.EditText' and @bounds='[609,501][1314,672]']"
		 * ).sendKeys("MohammedA"); driver.
		 * findElementByXPath("//*[@class='android.widget.EditText' and @bounds='[609,763][1314,934]']"
		 * ).sendKeys("AslamR"); //driver.
		 * findElementByXPath("//*[@class='android.widget.EditText' and @text='Enter your age here']"
		 * ).click(); //driver.
		 * findElementByXPath("//*[@class='android.widget.EditText' and @text='Enter your age here']"
		 * ).clear(); driver.
		 * findElementByXPath("//*[@class='android.widget.EditText' and @bounds='[609,1025][1314,1196]']"
		 * ).sendKeys("26");
		 */
		driver.findElementByXPath("//*[@class='android.widget.TextView' and @text='Male']").click();
		driver.findElementByXPath("//*[@class='android.widget.TextView' and @text='Save Profile']").click();
		System.out.println ("Profile Successfuly Saved");


		//I Feel good
		Thread.sleep(3000);
		driver.findElementByXPath("//*[@class='android.view.ViewGroup' and @index='4']").click();
		System.out.println ("Clicked on 'I feel good' Button ");


		//Daily Check-in Screen
		Thread.sleep(3000);
		driver.findElementByXPath("//*[@class='android.view.ViewGroup' and @index='3']").click();
		System.out.println ("Clicked on 'Go to Dashboard' Button ");
		System.out.println ("User Successfully Navigate on Dashboard");


		//Get Counter value


		// I don't feel well
		Thread.sleep(3000);
		driver.findElementByXPath("//*[@class='android.view.ViewGroup' and @index='5']").click();
		System.out.println ("Clicked on 'I don't feel well' Button ");
	}
	
	
	
}

