package com.mobileApp.clear360.utility;



import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.BeforeClass;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

//import com.sun.java.util.jar.pack.Package.File;

import io.appium.java_client.remote.MobileCapabilityType;

public class baseClassMobile {
	
	public static AppiumDriver<MobileElement> driver;
	public static AndroidDriver<MobileElement> androidDriver;
	//Method "caps" define to use in other class ( Inheritation )
   // public static AndroidDriver<AndroidElement> caps() throws MalformedURLException {
	
    	//public static void main(String[] args) throws MalformedURLException {
		// TODO Auto-generated method stub
	
	@BeforeClass
	public void setupapp() throws MalformedURLException	{
		//Create file object for access APK from any PC and anywhere
		File f = new File ("src");
		//File fs = new File(f, "Guardian_July14__stage.apk");
		File fs = new File(f, "Clear360EduMVP_dev_Sep23_v2.apk");
		
		
		DesiredCapabilities cap = new DesiredCapabilities();
		
		
		//cap.setCapability(MobileCapabilityType.DEVICE_NAME, "Aslam_pie");
		cap.setCapability(MobileCapabilityType.DEVICE_NAME, "Android Device");
		// cap.setCapability(MobileCapabilityType.UDID,"8cddcac4");
		 //cap.setCapability(MobileCapabilityType.PLATFORM_VERSION,"9.0");
		 cap.setCapability(MobileCapabilityType.APP, fs.getAbsolutePath());
		 //cap.setCapability("appPackage", "com.darwin.guardiantracker.staging");
		 cap.setCapability("appPackage", "com.darwin.clear.education");
		 //cap.setCapability("appActivity", "com.truworth.wellnesscorner.Main");
		cap.setCapability("RESET","true"); 
		
		
		 
		 //for connection to server link ( AndroidDriver<AndroidElement> = data type  of driver variable ) 
		//AndroidDriver driver = new AndroidDriver(connectiontoserverlink, cap); 
		URL url = new URL("http://127.0.0.1:4723/wd/hub");    
		driver = new AndroidDriver<MobileElement>(url,cap);
		//AndroidDriver <AndroidElement>  driver = new AndroidDriver<> (new URL("http://127.0.0.1:4723/wd/hub"),cap);
		 
         System.out.println("Launch Clear360 App"); 
		 
		 
		 }

    
    public WebDriver getDriver() {
    	
    	return driver;
    	
    }
    
    
    
    
}
