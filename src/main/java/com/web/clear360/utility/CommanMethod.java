package com.web.clear360.utility;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class CommanMethod {
	
	public static WebDriver driver ;
	
	static String FirstName_Error = "First name is invalid in the CSV file at row : 2";
	static String LastName_Error = "Last name is invalid in the CSV file at row : 3";
	static String DOB_Error = "Date of birth is required in the CSV file at row : 4";
	static String Location_Error = "School/Location is required in the CSV file at row : 5";
	static String Location_Error1 = "School/Location is required in the CSV file at row : 6";
	static String DuplicateMobile_Error = "Duplicate mobile in the CSV file at row : 7";
	static String DuplicateMobile_Error1 = "Duplicate mobile in the CSV file at row : 8";
	static String GuardianName_Error = "Delegate / Guardian is not present in the CSV file at row : 9";
	static String GuardianName_Error1 = "Delegate / Guardian is not present in the CSV file at row : 10";
	static String FirstName_Error1 = "First name is invalid in the CSV file at row : 11";
	static String LastName_Error1 = "Last name is invalid in the CSV file at row : 12";
	static String FirstName_Error2 = "First name is invalid in the CSV file at row : 13";
	static String LastName_Error2 = "Last name is invalid in the CSV file at row : 14";
	static String MobileNumber_Error = "Mobile number is invalid in the CSV file at row";
	static String UniqueId_Error = "Unique ID is invalid in the CSV file at row : 16";
	static String CSVEmpty_Error = "CSV contains empty rows";
	static String Successfully_Error = "1 record(s) added successfully";
	
	

		public static void fileUpload(String filenameupload) throws IOException {
			
			Runtime.getRuntime().exec("F:\\AurigaIT\\Web\\CSV\\FileUploads.exe"+" "+filenameupload);

		}
		
		public static void errorCheck(WebDriver driver) {
			
			String ActualText1 = driver.findElement(By.xpath("//div[@class='alert alert-warning alert-dismissible fade show']")).getText();
			
			if (ActualText1.contains(FirstName_Error)) {
				System.out.println("First Name invalid in CSV");	
			}
	
			if(ActualText1.contains(LastName_Error)) {
				System.out.println("Last Name invalid in CSV");	
			}
			
			if(ActualText1.contains(DOB_Error)) {
				System.out.println("Date of birth is required in the CSV");	
			}
			
			if(ActualText1.contains(Location_Error)) {
				System.out.println("School/Location is required in the CSV");	
			}
			
			if(ActualText1.contains(Location_Error1)) {
				System.out.println("School/Location is required in the CSV");	
			}
			
			if(ActualText1.contains(DuplicateMobile_Error)) {
				System.out.println("Duplicate mobile in the CSV");	
			}
			
			if(ActualText1.contains(DuplicateMobile_Error1)) {
				System.out.println("Duplicate mobile in the CSV");	
			}
			
			if(ActualText1.contains(GuardianName_Error)) {
				System.out.println("Delegate / Guardian is not present in the CSV");	
			}
			
			if(ActualText1.contains(GuardianName_Error1)) {
				System.out.println("Delegate / Guardian is not present in the CSV");	
			}
			
			if(ActualText1.contains(FirstName_Error1)) {
				System.out.println("First Name invalid in CSV");	
			}
			
			if(ActualText1.contains(LastName_Error1)) {
				System.out.println("Last Name invalid in CSV");	
			}
			
			if(ActualText1.contains(FirstName_Error2)) {
				System.out.println("First Name invalid in CSV");	
			}
			
			if(ActualText1.contains(LastName_Error2)) {
				System.out.println("Last Name invalid in CSV");	
			}
			
			if(ActualText1.contains(MobileNumber_Error)) {
				System.out.println("Mobile number is invalid in the CSV");	
			}
			
			if(ActualText1.contains(UniqueId_Error)) {
				System.out.println("Unique ID is invalid in the CSV");	
			}
			
			if(ActualText1.contains(CSVEmpty_Error)) {
				System.out.println("CSV contains empty rows");	
			}
			
			if(ActualText1.contains(Successfully_Error)) {
				System.out.println("Added successfully");	
			}
			
		}
		
		
          public static void checkNoerror() {
			
		}
		
		
		
		
	}

	
