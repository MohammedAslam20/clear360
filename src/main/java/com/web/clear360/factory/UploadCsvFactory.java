package com.web.clear360.factory;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import com.web.clear360.utility.BaseclassWeb;

public class UploadCsvFactory extends BaseclassWeb{

	//This is a constructor, as every page need a base driver to find web elements
	public UploadCsvFactory(WebDriver driver) {
		super();
	}
	

	@FindBy(how = How.XPATH, using = "//div[@class='avatar-w']//i[@class='os-icon os-icon-menu']")     
	public WebElement click_Menu ;
	
	@FindBy(how = How.XPATH, using = "//div[@class='logged-user-i logged-user-i1']//span[contains(text(),'Manage Users')]")     
	public WebElement tab_ManageUsers ;
	
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Students')]")     
	public WebElement tab_Students ;
	
	@FindBy(how = How.XPATH, using = "//button[contains(text(),'Upload CSV File')]")     
	public WebElement button_UploadCSV ;
	
	@FindBy(how = How.XPATH, using = "//div[@class='text-center cancel-upload-btns']//button[@class='btn btn-primary'][contains(text(),'Upload')]")     
	public WebElement button_Upload ;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Staff')]")     
	public WebElement tab_Staff ;
	
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Faculty')]")     
	public WebElement tab_Faculty ;
	
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Guardians')]")     
	public WebElement tab_Guardians ;
	
	
	
	public WebElement getClick_Menu() {
		return click_Menu;
	}

	public void setClick_Menu(WebElement click_Menu) {
		this.click_Menu = click_Menu;
	}

	public WebElement getTab_ManageUsers() {
		return tab_ManageUsers;
	}

	public void setTab_ManageUsers(WebElement tab_ManageUsers) {
		this.tab_ManageUsers = tab_ManageUsers;
	}

	public WebElement getTab_Students() {
		return tab_Students;
	}

	public void setTab_Students(WebElement tab_Students) {
		this.tab_Students = tab_Students;
	}

	public WebElement getButton_UploadCSV() {
		return button_UploadCSV;
	}

	public void setButton_UploadCSV(WebElement button_UploadCSV) {
		this.button_UploadCSV = button_UploadCSV;
	}

	public WebElement getButton_Upload() {
		return button_Upload;
	}

	public void setButton_Upload(WebElement button_Upload) {
		this.button_Upload = button_Upload;
	}

	public WebElement getTab_Staff() {
		return tab_Staff;
	}

	public void setTab_Staff(WebElement tab_Staff) {
		this.tab_Staff = tab_Staff;
	}

	public WebElement getTab_Faculty() {
		return tab_Faculty;
	}

	public void setTab_Faculty(WebElement tab_Faculty) {
		this.tab_Faculty = tab_Faculty;
	}

	public WebElement getTab_Guardians() {
		return tab_Guardians;
	}

	public void setTab_Guardians(WebElement tab_Guardians) {
		this.tab_Guardians = tab_Guardians;
	}

	
	
	
}
